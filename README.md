# README

==**NOTE**== This project has been archived! The GBIF data validator project has been superseded. Validation is now done within the Pipelines project: https://github.com/gbif/pipelines




## Using the GBIF data validator

A simple project for creating a docker container containing the GBIF Data Validator (https://github.com/gbif/gbif-data-validator)


## Running the GBIF data validator

First of all, you do not need to run the gbif data validator as a docker container. You can of course build and run it on your local machine. The following instructions will get you on the way.

Prerequisites:
- java version >= 1.8.0
- maven version >= 3.6.0


Building the validator:

```bash
$ git clone https://github.com/gbif/gbif-data-validator

$ mvn clean package install -U
```


Move to the validator webservice directory:

```bash
$ cd validator-ws
```

Create an application properties file `validation.properties` in src/main/resources with this content:

```
validation.apiUrl=http://api.gbif.org/v1/
validation.apiDataValidationPath=http://api.gbif.org/v1/jobserver
validation.extensionDiscoveryUrl=http://rs.gbif.org/extensions.json
validation.workingDir=/tmp/validator
validation.jobResultStorageDir=/tmp/validator/store
validation.fileSplitSize=10000
validation.maxFileTransferSizeInBytes=250000000
```

Now run the validation service:
``` 
$ mvn jetty:run
```


You can validate a dwc archive (from a different terminal) like this:

```bash
$ ./run path/to/your/dwc/archive

$ ./run-local src/test/resources/dwca/Archive.zip
```


## Running the GBIF data validator as a docker container

You can save yourself some trouble (and time) by using a docker image of the gbif data validator:

```bash
$ docker pull registry.gitlab.com/naturalis/bii/colander/gbif-data-validator:latest

$ docker run --name validator --publish 8080:8080 --volume ~/dwca:/home/dwca --detach gbif-data-validator

$ docker exec -it validator ./run-local /home/dwca/mammalia-20200512.dwca.zip

$ curl localhost:8080/jobserver/status/1589299013844
```


Clean up afterwards:
```bash
$ docker stop validator

$ docker rm validator
```


## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

* `pre-commit autoupdate`
* `pre-commit install`
